package pl.trpaslik.babydraw;

import android.content.Context;
import android.widget.ImageView;
import android.widget.Toast;

class EraserToast extends Toast {
	private ImageView view;

	public EraserToast(Context context) {
		super(context);
		view = new ImageView(context);
		setView(view);
	}

	public void setId(int id) {
		view.setImageResource(id);
	}

}
