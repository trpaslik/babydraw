/*
    This file is part of Baby Draw.

    Baby Draw is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Baby Draw is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Baby Draw.  If not, see <http://www.gnu.org/licenses/>.

 */

package pl.trpaslik.babydraw;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BlurMaskFilter;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

public class DrawActivity extends Activity {

	private static final int THIN_PENCIL_WIDTH = 6;
	private Paint paint;
	private int width = 1;
	private DrawView drawView;
	private ColorToast colorToast;
	private EraserToast eraserToast;
	private ColorPencil colorPencil;
	private View colorsView;
	private PopupWindow colorsPopupWindow;
	private Map<ImageButton, Integer> colorsMap;
	private File tmpFile;
	private Random random = new Random();

	private static final int EVENT_LIMIT = 500; // milliseconds
	private static final int PIC_SHARED = 41241; // any int

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// hide application title
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		// prevent screen dim
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		// set full screen
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

		colorToast = new ColorToast(this);
		colorToast.setDuration(Toast.LENGTH_SHORT);
		eraserToast = new EraserToast(this);
		eraserToast.setDuration(Toast.LENGTH_SHORT);
		eraserToast.setId(R.drawable.eraser);

		paint = new Paint();
		paint.setAntiAlias(true);
		paint.setDither(true);
		paint.setColor(Color.BLUE);
		paint.setStyle(Paint.Style.STROKE);
		paint.setStrokeJoin(Paint.Join.ROUND);
		paint.setStrokeCap(Paint.Cap.ROUND);
		paint.setStrokeWidth(width * THIN_PENCIL_WIDTH);

		paint.setMaskFilter(new BlurMaskFilter(2, BlurMaskFilter.Blur.NORMAL));

		drawView = new DrawView(this);
		drawView.setPaint(paint);
		drawView.setOnOptionsEvent(new Runnable() {

			@Override
			public void run() {
				colorToast.cancel();
				pickPredefinedPencilColor();
			}

		});
		setContentView(drawView);
		colorPencil = new ColorPencil(this);
		Toast.makeText(this, "Long press screen", Toast.LENGTH_LONG).show();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_N) { // normal
			drawView.renderMode('n');
			drawView.invalidate();
		} else if (keyCode == KeyEvent.KEYCODE_M) { // mask
			drawView.renderMode('m');
			drawView.invalidate();
		} else if (keyCode == KeyEvent.KEYCODE_P) { // prev
			drawView.renderMode('p');
			drawView.invalidate();
		} else if (keyCode == KeyEvent.KEYCODE_U) { // undo
			drawView.renderMode('u');
			drawView.invalidate();
		}

		if (colorsPopupWindow != null && colorsPopupWindow.isShowing()) {
			colorsPopupWindow.dismiss();
			drawView.setEnabled(true);
			return true;
		}
		if (keyCode == KeyEvent.KEYCODE_MENU) {
			colorToast.cancel();
			eraserToast.cancel();
			pickPredefinedPencilColor();
		} else if (keyCode == KeyEvent.KEYCODE_BACK) {
			if (event.getEventTime() - drawView.getLastEventTime() > EVENT_LIMIT) {
				drawView.clear();
			}
		} else if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
			saveScreenShot(null, false);
		} else if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
			try {
				sharePicture();
			} catch (IOException e) {
				Log.e("Share", e.toString());
			}
		}
		// ignore other key down events
		return true;
	}

	private void sharePicture() throws IOException {
		Intent share = new Intent(Intent.ACTION_SEND);
		share.setType("image/jpeg");
		File dir = getExternalCacheDir();
		dir.mkdirs();
		tmpFile = new File(dir, pictureFileName());
		Log.d("Share", "Sharing temp file " + tmpFile.getAbsolutePath());
		Toast.makeText(this, R.string.picture_sharing, Toast.LENGTH_SHORT)
				.show();
		saveScreenShot(tmpFile, true);
		share.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(tmpFile));
		startActivityForResult(Intent.createChooser(share, "Share Image"),
				PIC_SHARED);
	}

	private String pictureFileName() {
		final Date now = new Date();
		// we must avoid ":". It causes problems on some devices
		final String fmt = "BabyDraw-%tF %tH-%tM-%tS.jpg";
		return String.format(fmt, now, now, now, now);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == PIC_SHARED) {
			// delete temporary file after sharing is done
			// tmpFile.delete();
			//
			// we cannot delete it now. Some sharing apps like google drive
			// use background services to upload files. It might be to early to
			// delete it and upload will fail.
			//
			// TODO: clean up OLD temp files (older than 7 days?)
		}
	}

	private ImageButton imgBut(int resId) {
		return (ImageButton) colorsView.findViewById(resId);
	}

	private void pickPredefinedPencilColor() {
		if (colorsPopupWindow != null && colorsPopupWindow.isShowing()) {
			return;
		}
		if (colorsView == null) {
			LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			colorsView = inflater.inflate(R.layout.colors_dialog, null, false);
			colorsView.setBackgroundColor(Color.argb(192, 160, 160, 160));
		}
		TextView message = (TextView) colorsView
				.findViewById(R.id.textViewMessage);
		message.setText(getRandomMessage());
		if (colorsPopupWindow == null) {
			colorsPopupWindow = new PopupWindow(colorsView);
			colorsPopupWindow.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
			colorsPopupWindow
					.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
		}

		if (colorsMap == null) {
			int[] tmp = new int[] { R.id.imageButton1, R.id.imageButton2,
					R.id.imageButton3, R.id.imageButton4, R.id.imageButton5,
					R.id.imageButton6, R.id.imageButton7, R.id.imageButton8,
					R.id.imageButton9, R.id.imageButton10, R.id.imageButton11,
					R.id.imageButton12, R.id.imageButton13, R.id.imageButton14,
					R.id.imageButton15 };
			int cols[] = new int[] { 0xCCFFFFFF, 0xCC808080, 0xCC000000,
					0xCCFF0000, 0xCC800000, 0xCCFFFF00, 0xCC808000, 0xCC00FF00,
					0xCC008000, 0xCC00FFFF, 0xCC008080, 0xCC0000FF, 0xCC000080,
					0xCCFF00FF, 0xCC800080 };

			colorsMap = new HashMap<ImageButton, Integer>();
			for (int i = 0; i < tmp.length; i++) {
				colorsMap.put(imgBut(tmp[i]), cols[i]);
			}

			OnClickListener pencilClickListener = new OnClickListener() {

				@Override
				public void onClick(View v) {
					ImageButton but = (ImageButton) v;
					colorsPopupWindow.dismiss();
					drawView.setEnabled(true);
					changePencilColor(colorsMap.get(but));
				}
			};
			for (ImageButton but : colorsMap.keySet()) {
				but.setOnClickListener(pencilClickListener);
				but.setImageBitmap(colorPencil.getBitmap(colorsMap.get(but)));
			}
		}

		ImageButton eraserBut = imgBut(R.id.imageButtonEraser);
		eraserBut.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				colorsPopupWindow.dismiss();
				drawView.setEnabled(true);
				setEraserMode(true);
			}
		});

		ImageButton undoBut = imgBut(R.id.imageButtonUndo);
		undoBut.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				colorsPopupWindow.dismiss();
				drawView.setEnabled(true);
				drawView.undo();
			}
		});

		class WidthClickListener implements OnClickListener {
			private int width;

			WidthClickListener(int width) {
				this.width = width;
			}

			@Override
			public void onClick(View v) {
				colorsPopupWindow.dismiss();
				drawView.setEnabled(true);
				changeWidth(width);
			}

		}
		imgBut(R.id.imageButtonWidth1).setOnClickListener(
				new WidthClickListener(1));
		imgBut(R.id.imageButtonWidth2).setOnClickListener(
				new WidthClickListener(2));
		imgBut(R.id.imageButtonWidth3).setOnClickListener(
				new WidthClickListener(3));
		imgBut(R.id.imageButtonWidth4).setOnClickListener(
				new WidthClickListener(4));

		colorsPopupWindow.showAtLocation(drawView, Gravity.CENTER, 0, 0);
		drawView.setEnabled(false);
	}

	/**
	 * Change pencil paint to random color
	 */
	private void changePencilColor(int col) {
		drawView.setEraserMode(false);
		paint.setColor(col);
		colorToast.setColor(col);
		colorToast.show();
	}

	/**
	 * Change eraser or pencil width
	 */
	private void changeWidth(int width) {
		this.width = width;
		updateWidth();
		drawView.setPaint(paint);
	}

	private void setEraserMode(boolean eraserMode) {
		drawView.setEraserMode(eraserMode);
		updateWidth();
		eraserToast.show();
	}

	private void updateWidth() {
		drawView.setEraserWidth(width * THIN_PENCIL_WIDTH * 2);
		paint.setStrokeWidth(width * THIN_PENCIL_WIDTH);
	}

	/**
	 * save DrawView bitmap as a JPG file in PICTURES directory or to specified
	 * file path if jpgFile is not null.
	 */
	private void saveScreenShot(File jpgFile, boolean silent) {
		if (!silent) {
			Toast.makeText(this, R.string.picture_saving, Toast.LENGTH_SHORT)
					.show();
		}
		if (jpgFile == null) {
			File destDir = Environment
					.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
			destDir.mkdirs();
			jpgFile = new File(destDir, pictureFileName());
		}
		Bitmap bitmap = drawView.getBitmap();
		Log.d("Save", "Save picture: " + jpgFile.getAbsolutePath());
		try {
			OutputStream stream = new FileOutputStream(jpgFile);
			bitmap.compress(CompressFormat.JPEG, 100, stream);
			stream.close();
			if (!silent) {
				Toast.makeText(this, R.string.picture_saved, Toast.LENGTH_SHORT)
						.show();
			}
			scanFile(jpgFile);
		} catch (IOException e) {
			Log.e("Save", e.getMessage());
			e.printStackTrace();
		}
	}

	private String getRandomMessage() {
		String[] messages = getResources().getStringArray(R.array.messages);
		return messages[random.nextInt(messages.length)];
	}

	/**
	 * Ask System to scan a file via MediaScanner service
	 */
	private void scanFile(File downloadedFile) {
		Intent intent = new Intent(
				"android.intent.action.MEDIA_SCANNER_SCAN_FILE");
		intent.setData(Uri.fromFile(downloadedFile));
		sendBroadcast(intent);
	}

}
