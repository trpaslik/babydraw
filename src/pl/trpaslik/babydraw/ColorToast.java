/*
    This file is part of Baby Draw.

    Baby Draw is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Baby Draw is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Baby Draw.  If not, see <http://www.gnu.org/licenses/>.

 */

package pl.trpaslik.babydraw;

import android.content.Context;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

class ColorToast extends Toast {
	private ImageView view;
	private ColorPencil colorPencil;

	public ColorToast(Context context) {
		super(context);
		colorPencil = new ColorPencil(context);
		view = new ImageView(context);
		setView(view);
	}

	public void setColor(int color) {
		try {
			view.setImageBitmap(colorPencil.getBitmap(color));
		} catch (Exception e) {
			Log.e("Bitmap", "Cannot decode bitmap. OutOfMemory?");
		}
	}

}
