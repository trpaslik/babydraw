/*
    This file is part of Baby Draw.

    Baby Draw is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Baby Draw is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Baby Draw.  If not, see <http://www.gnu.org/licenses/>.

 */

package pl.trpaslik.babydraw;


public class Surface {

	/**
	 * list of possible backgrounds
	 */
	public static int backgrounds[] = { R.drawable.bg_00, R.drawable.bg_01,
			R.drawable.bg_02, R.drawable.bg_03, R.drawable.bg_04,
			R.drawable.bg_05, R.drawable.bg_06, R.drawable.bg_07, };

	/**
	 * index of current background on this surface
	 */
	private int currentIndex = 0;

	public int getResourceId() {
		return backgrounds[currentIndex];
	}

	public void setNext() {
		currentIndex = (currentIndex + 1) % backgrounds.length;
	}

}
