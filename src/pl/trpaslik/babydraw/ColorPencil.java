/*
    This file is part of Baby Draw.

    Baby Draw is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Baby Draw is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Baby Draw.  If not, see <http://www.gnu.org/licenses/>.

 */

package pl.trpaslik.babydraw;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Build;

class ColorPencil {

	static Bitmap alphaBitmap;
	private Context ctx;

	ColorPencil(Context ctx) {
		this.ctx = ctx;
	}

	@TargetApi(19)
	private void setPremultiplied(Bitmap bitmap) {
		if (Build.VERSION.SDK_INT >= 19) {
			// seems to be required on kitkat
			bitmap.setPremultiplied(true);
		}
	}

	Bitmap getBitmap(int color) {
		if (alphaBitmap == null) {
			alphaBitmap = BitmapFactory.decodeResource(ctx.getResources(),
					R.drawable.pencil).extractAlpha();
		}
		Bitmap bitmap = alphaBitmap.copy(Bitmap.Config.ARGB_8888, true);
		setPremultiplied(bitmap);
		Canvas canvas = new Canvas(bitmap);
		Paint paint = new Paint();
		paint.setColor(color);
		canvas.drawBitmap(alphaBitmap, 0, 0, paint);
		return bitmap;
	}

}
