/*
    This file is part of Baby Draw.

    Baby Draw is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Baby Draw is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Baby Draw.  If not, see <http://www.gnu.org/licenses/>.

 */

package pl.trpaslik.babydraw;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Shader;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

public class DrawView extends View implements View.OnLongClickListener {

	// surface background
	private Bitmap bgBitmap; // store only clear surface in 565
	private Canvas bgCanvas;
	private Surface surface;
	private boolean rebuildBgNeeded;

	// already drawn part of the image in 8888 (without current path on it)
	private Bitmap prevBitmap;
	private Canvas prevCanvas;

	// rasterized current path in alpha channel to be used as mask
	// we draw black on it and DST_OUT compose on prevBitmap to erase
	private Bitmap maskBitmap;
	private Canvas maskCanvas;

	private Bitmap undoBitmap;
	private Canvas undoCanvas;

	private Path currentPath; // currently drawing path
	private Paint currPathPaint; // paint for path for non-eraser mode

	private float lastX, lastY;
	private static final float TOUCH_TOLERANCE = 12;
	private boolean drawOnlyPoint;
	private long lastEventTime;
	private Context ctx;
	private Runnable onOptionsEvent;

	// how many times [back] was pressed since last screen touch
	private int backPressed;

	private boolean eraserMode = false;

	// used to erase mask from prevBitmap when eraser mode
	private final Paint xFerPaint;
	private Paint eraserPaint; // with specified eraser stroke width
	private char mode = 'n'; // for debug - render mode
								// (n)ormal/(u)ndo/(m)ask/(p)rev)
	private Paint redPaint = new Paint();

	public DrawView(Context c) {
		super(c);
		redPaint.setColor(Color.RED);
		redPaint.setTextSize(20);
		ctx = c;
		currentPath = new Path();
		currPathPaint = new Paint();
		setOnLongClickListener(this);
		surface = new Surface();
		xFerPaint = new Paint();
		// remove from DST==prevBitmap pixels defined in SRC==maskBitmap
		xFerPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OUT));
		eraserPaint = new Paint();
		eraserPaint.setColor(Color.WHITE);
		eraserPaint.setStyle(Paint.Style.STROKE);
		eraserPaint.setStrokeJoin(Paint.Join.ROUND);
		eraserPaint.setStrokeCap(Paint.Cap.ROUND);
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);
		// allocate bitmaps

		// surface background (used to compose and to erase)
		bgBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.RGB_565);
		bgCanvas = new Canvas(bgBitmap);

		// all finished rasterized path (without background)
		prevBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
		prevCanvas = new Canvas(prevBitmap);

		// to rasterize current path and compose with rest
		maskBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ALPHA_8);
		maskCanvas = new Canvas(maskBitmap);

		// to store previous bitmap in case of undo (always before prevBitmap)
		undoBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
		undoCanvas = new Canvas(undoBitmap);

		// clear bitmaps
		clear();
	}

	/**
	 * clear all bitmaps
	 */
	public void clear() {
		backPressed++;
		// when 1st time clicked (since touch), clear using same surface
		if (backPressed > 1) {
			// more multiple [back] press, switches surface
			surface.setNext();
		}
		// prepare new background bitmap
		drawSurfaceOnBackground(surface.getResourceId());

		undoSavepoint();

		// set prevBitmap to transparent
		prevCanvas.drawColor(Color.TRANSPARENT, Mode.SRC);

		// set current bitmap to transparent
		maskCanvas.drawColor(Color.TRANSPARENT, Mode.SRC);

		// redraw view
		invalidate();
	}

	/**
	 * draw surface image on background bitmap
	 */
	private void drawSurfaceOnBackground(int resId) {
		Bitmap resBmp = BitmapFactory.decodeResource(ctx.getResources(), resId);
		BitmapShader bs = new BitmapShader(resBmp, Shader.TileMode.REPEAT,
				Shader.TileMode.REPEAT);
		Paint tilePaint = new Paint();
		tilePaint.setShader(bs);
		bgCanvas.drawPaint(tilePaint);
	}

	@Override
	protected void onDraw(Canvas canvas) {
		if (rebuildBgNeeded) {
			drawSurfaceOnBackground(surface.getResourceId());
			rebuildBgNeeded = false;
		}
		if (mode == 'n') { // normal mode
			canvas.drawBitmap(bgBitmap, 0, 0, null);
			if (eraserMode) {
				if (!drawOnlyPoint) {
					// 1. make maskBitmap transparent
					maskCanvas.drawColor(Color.TRANSPARENT, Mode.SRC);
					// 2. draw path on maskBitmap using black paint
					maskCanvas.drawPath(currentPath, eraserPaint);
				}
				// 3. compose DST_OUT currBitmap on prevBitmap
				prevCanvas.drawBitmap(maskBitmap, 0, 0, xFerPaint);
				// draw erased prevBitmap
				canvas.drawBitmap(prevBitmap, 0, 0, null);
			} else /* normal pencil */{
				canvas.drawBitmap(prevBitmap, 0, 0, null);
				canvas.drawPath(currentPath, currPathPaint);
			}
		} else if (mode == 'm') { // mask only
			canvas.drawColor(Color.WHITE);
			canvas.drawBitmap(maskBitmap.extractAlpha(), 0, 0, null);
			canvas.drawText("MODE: MASK", 50, 450, redPaint);
		} else if (mode == 'p') { // prev only
			canvas.drawBitmap(prevBitmap, 0, 0, null);
			canvas.drawText("MODE: PREV", 50, 450, redPaint);
		} else if (mode == 'u') { // undo only
			canvas.drawBitmap(undoBitmap, 0, 0, null);
			canvas.drawText("MODE: UNDO", 50, 450, redPaint);
		}
	}

	private void touchStart(float x, float y) {
		currentPath.reset();
		drawOnlyPoint = true;
		currentPath.moveTo(x, y);
		lastX = x;
		lastY = y;
		// undoSavepoint();
	}

	private void touchMove(float x, float y) {
		float dx = Math.abs(x - lastX);
		float dy = Math.abs(y - lastY);

		if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
			currentPath.quadTo(lastX, lastY, (x + lastX) / 2, (y + lastY) / 2);
			drawOnlyPoint = false;
			lastX = x;
			lastY = y;
		}
	}

	private void touchUp() {
		if (eraserMode) {
			if (drawOnlyPoint) {
				Log.e("XXX", "case1 E P");
				undoSavepoint();
				maskCanvas.drawPoint(lastX, lastY, eraserPaint);
			} else {
				Log.e("XXX", "case2 E L");
				currentPath.lineTo(lastX, lastY);
				// undoSavepoint();
			}
		} else { // regular pencil mode
			if (drawOnlyPoint) {
				Log.e("XXX", "case3 P P");
				undoSavepoint();
				prevCanvas.drawPoint(lastX, lastY, currPathPaint);
			} else {
				Log.e("XXX", "case4 P L");
				currentPath.lineTo(lastX, lastY);
				undoSavepoint();
				prevCanvas.drawPath(currentPath, currPathPaint);
			}
		}
		currentPath.reset();
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (!isEnabled()) {
			return true;
		}
		float x = event.getX();
		float y = event.getY();
		lastEventTime = event.getEventTime();
		backPressed = 0;

		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			touchStart(x, y);
			invalidate();
			break;
		case MotionEvent.ACTION_MOVE:
			touchMove(x, y);
			invalidate();
			break;
		case MotionEvent.ACTION_UP:
			touchUp();
			invalidate();
			break;
		}
		return super.onTouchEvent(event);
	}

	public void setPaint(Paint paint) {
		this.currPathPaint = paint;
		eraserPaint.setMaskFilter(paint.getMaskFilter());
	}

	public long getLastEventTime() {
		return lastEventTime;
	}

	public Bitmap getBitmap() {
		// this trick prevents from allocating new big bitmap.
		// we call getBimap() rarely, so we can reuse bgBitmap
		// and later rebuild it
		this.rebuildBgNeeded = true;
		bgCanvas.drawBitmap(prevBitmap, 0, 0, null);
		return bgBitmap;
	}

	/**
	 * @param r
	 *            Runnable to be called when user long-press the view
	 */
	public void setOnOptionsEvent(Runnable r) {
		onOptionsEvent = r;
	}

	@Override
	public boolean onLongClick(View v) {
		// only long-click in a place (ignore long-drawings)
		if (drawOnlyPoint) {
			// make sure we don't paint a dot
			// when invoking an option window
			drawOnlyPoint = false;
			if (onOptionsEvent != null) {
				onOptionsEvent.run();
				return true;
			}
		}
		return false;
	}

	public boolean isEraserMode() {
		return eraserMode;
	}

	public void setEraserMode(boolean eraserMode) {
		this.eraserMode = eraserMode;
	}

	public void setEraserWidth(float eraserWidth) {
		eraserPaint.setStrokeWidth(eraserWidth);
	}

	private void undoSavepoint() {
		Log.e("XXX", "SAVEPOINT");
		undoCanvas.drawColor(Color.TRANSPARENT, Mode.SRC);
		undoCanvas.drawBitmap(prevBitmap, 0, 0, null);
	}

	public void undo() {
		Log.e("XXX", "UNDO");
		// swap undo and prev buffers
		Bitmap tmpB = prevBitmap;
		Canvas tmpC = prevCanvas;
		prevBitmap = undoBitmap;
		prevCanvas = undoCanvas;
		undoBitmap = tmpB;
		undoCanvas = tmpC;
		invalidate();
	}

	void renderMode(char i) {
		Log.e("XXX", "render mode: " + i);
		mode = i;
	}
}
